import React, { useState } from 'react';
import Head from 'next/head';
import styled from 'styled-components';

const Header = styled.div`
  height: 40px;
  width: 100%;
  position: relative;
  top: 0;
  background-color: ${({isBlack}) => isBlack ? 'black' : 'red'};
`;

const Home = () => {
  const [state, setState] = useState(false);
  return (
    <div className="container">
      <Head>
        <title>Create Next App</title>
        <link rel="icon" href="/favicon.ico"/>
      </Head>
      <Header isBlack={state} />
      <button onClick={() => setState(!state)}>{state ? 'красный' : 'черный'}</button>
    </div>
  )
}

export default Home
